package com.appfuxion.hr.timesheetservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("stag")
@SpringBootTest
class TimesheetServiceApplicationTests {

    @Test
    void contextLoads() {
    }

}
