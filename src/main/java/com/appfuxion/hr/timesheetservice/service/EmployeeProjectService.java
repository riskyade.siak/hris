package com.appfuxion.hr.timesheetservice.service;

import com.appfuxion.hr.timesheetservice.repository.EmployeeProjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EmployeeProjectService {

    private final EmployeeProjectRepository repository;

    public EmployeeProjectService(EmployeeProjectRepository repository) {
        this.repository = repository;
    }
}
