package com.appfuxion.hr.timesheetservice.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record AuthenticationResponse (
        @JsonProperty("access_token")
        String accessToken,
        @JsonProperty("refresh_token")
        String refreshToken,
        @JsonProperty(value = "client")
        ClientResponse client,
        @JsonProperty(value = "employee")
        EmployeeResponse employee,
        @JsonProperty(value = "project_list")
        Set<ProjectResponse> projectList,
        @JsonProperty(value = "role_type")
        String roleType,
        @JsonProperty(value = "user")
        UserResponse user
        ) implements Serializable {
}
