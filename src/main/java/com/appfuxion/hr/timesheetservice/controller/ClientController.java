package com.appfuxion.hr.timesheetservice.controller;

import com.appfuxion.hr.timesheetservice.payload.CommonResponse;
import com.appfuxion.hr.timesheetservice.payload.request.ClientRequest;
import com.appfuxion.hr.timesheetservice.service.ClientService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("clients")
@Slf4j
public class ClientController {

    private final ClientService service;
    private final ObjectMapper mapper = new ObjectMapper();

    public ClientController(ClientService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<CommonResponse> addDataClient(@Valid @RequestBody ClientRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method addDataClient on class %s with payload %s", ClientController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.addDataClient(request);
        log.info(String.format("Leaving method addDataClient on class %s", ClientController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<CommonResponse> getAllClients() throws JsonProcessingException {
        log.info(String.format("Entering method getAllClients on class %s", ClientController.class.getName()));
        CommonResponse response = service.listAllClients();
        log.info(String.format("Entering method getAllClients on class %s with response %s", ClientController.class.getName(), mapper.writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
