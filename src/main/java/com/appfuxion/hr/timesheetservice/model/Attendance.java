package com.appfuxion.hr.timesheetservice.model;

import com.appfuxion.hr.timesheetservice.model.audit.UserDateAudit;
import com.appfuxion.hr.timesheetservice.util.EventType;
import com.appfuxion.hr.timesheetservice.util.Workplace;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "attendances")
public class Attendance extends UserDateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "attendance_id", nullable = false)
    private UUID attendanceId;

    private LocalDateTime clockIn;

    private LocalDateTime clockOut;

    private LocalDate attendanceDate;

    @Column(name = "workplace")
    @Enumerated(EnumType.STRING)
    private Workplace workplace;

    @Column(name = "event")
    @Enumerated(EnumType.STRING)
    private EventType event;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "project_id", nullable = false)
    @JsonIgnore
    private Project project;

    private String dailyTask;

    private Long totalWork;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "employee_id", nullable = false)
    @JsonIgnore
    private Employee employee;
}
