package com.appfuxion.hr.timesheetservice.repository;

import com.appfuxion.hr.timesheetservice.model.Client;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Hidden
public interface ClientRepository extends JpaRepository<Client, Long> {
    boolean existsByClientName(String clientName);
}
