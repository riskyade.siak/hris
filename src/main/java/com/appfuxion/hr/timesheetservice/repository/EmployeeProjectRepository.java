package com.appfuxion.hr.timesheetservice.repository;

import com.appfuxion.hr.timesheetservice.model.Employee;
import com.appfuxion.hr.timesheetservice.model.EmployeeProject;
import com.appfuxion.hr.timesheetservice.model.EmployeeProjectKey;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Hidden
public interface EmployeeProjectRepository extends JpaRepository<EmployeeProject, EmployeeProjectKey> {
    List<EmployeeProject> findByEmployeeAndIsActive(Employee employee, boolean isActive);
}
