package com.appfuxion.hr.timesheetservice.repository;

import com.appfuxion.hr.timesheetservice.model.Job;
import com.appfuxion.hr.timesheetservice.model.JobHistoryKey;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Hidden
public interface JobHistoryRepository extends JpaRepository<Job, JobHistoryKey> {
}
