package com.appfuxion.hr.timesheetservice.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ProjectRequest implements Serializable {

    @NotBlank
    @JsonProperty(value = "project_name")
    private String projectName;

    @JsonProperty(value = "manager_id")
    private Long managerId;

    @JsonProperty(value = "client_id")
    private Long clientId;
}
