package com.appfuxion.hr.timesheetservice.repository;

import com.appfuxion.hr.timesheetservice.model.Attendance;
import com.appfuxion.hr.timesheetservice.model.Employee;
import com.appfuxion.hr.timesheetservice.model.Project;
import com.appfuxion.hr.timesheetservice.payload.AttendanceLogInterface;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@Hidden
public interface AttendanceRepository extends JpaRepository<Attendance, UUID>  {
    Optional<Attendance> findByAttendanceDateAndEmployeeAndProjectAndClockInIsNotNullOrderByClockInDesc(LocalDate attendanceDate, Employee employee, Project project);
	List<Attendance> findByEmployeeAndTotalWorkIsNotNull(Employee employee);
    List<Attendance> findByCreatedBy(String email);
    @Query("SELECT a FROM Attendance a WHERE CAST(a.clockIn AS date) BETWEEN :start AND :end ORDER BY a.createdBy asc")
    List<Attendance> getAttendanceByDateRange(@Param("start") LocalDate start,
                                              @Param("end") LocalDate end);

    @Query(value="SELECT DISTINCT a as attendance, a.attendance_id, a.clock_in, a.clock_out, \n" +
            "\ta.workplace, a.event, a.project_id, a.daily_task, \n" +
            "\ta.total_work, a.employee_id,\n" +
            "\twd.date as attendance_date, a.created_date \n" +
            "FROM timesheet.attendances a \n" +
            "\tRIGHT OUTER JOIN timesheet.employees e1 ON a.employee_id = e1.employee_id  \n" +
            "\tCROSS JOIN (\n" +
            "\t\tSELECT * FROM UNNEST(cast(:workingDays as date[])) AS day(date) \n" +
            "\t) AS wd \n" +
            "WHERE 1=1 OR :endOfDay = null OR :startOfDay = null  \n" +
            "\tAND :statusList IS NOT NULL AND a.event in :statusList \n" +
            "GROUP BY a.attendance_id, a.clock_in, a.clock_out, \n" +
            "\ta.workplace, a.event, a.project_id, a.daily_task, \n" +
            "\ta.total_work, a.employee_id, \n" +
            "\twd.date, a.created_date \n",
            nativeQuery = true)
    Page<AttendanceLogInterface> getAttendanceByDateRangeAndStatusWithPagination(@Param("statusList") List<String> statusList,
                                                                                 @Param("startOfDay") LocalTime startOfDay,
                                                                                 @Param("endOfDay") LocalTime endOfDay,
                                                                                 @Param("workingDays") LocalDate[] workingDays,
                                                                                 Pageable page);
}
