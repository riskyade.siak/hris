stages:
  - test
  - compile
  - build
  - tags
  - deploy

variables:
  STAG_PORT_SERVICE: 8101
  PROD_PORT_SERVICE: 8001

### TEST ###
stag-test:
  stage: test
  image: maven:latest
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn verify sonar:sonar -Dsonar.projectKey=appfuxion-my_hr_be_timesheet-service_AYfgo0_MMI5-YAvEAoUP -Dsonar.projectName='Timesheet Service' -Dmaven.repo.local=/app/.m2/repository
  allow_failure: true
  only:
    - main
  tags:
    - exabyte-docker

### COMPILE ###
stag-compile:
  stage: compile
  image: maven:latest
  environment: staging
  script:
    - mvn deploy -DskipTests -s ci_settings.xml -Dmaven.repo.local=/app/.m2/repository
  artifacts:
    when: always
    paths:
    - target/timesheet-service-0.0.1.jar
    expire_in: 1 week
  only:
    - stag
  tags:
    - exabyte-docker

prod-compile:
  stage: compile
  image: maven:latest
  environment: production
  before_script:
    - sed -i "s/spring.profiles.active=stag/spring.profiles.active=prod/g" "src/main/resources/application.properties"
  script:
    - mvn deploy -DskipTests -s ci_settings.xml -Dmaven.repo.local=/app/.m2/repository
  artifacts:
    when: always
    paths:
    - target/timesheet-service-0.0.1.jar
    expire_in: 1 week
  only:
    - /#prod/
  tags:
    - exabyte-docker

### BUILD ###
stag-build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - >
      docker build 
      --cache-from $CI_REGISTRY_IMAGE:latest 
      --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA 
      --tag $CI_REGISTRY_IMAGE:latest 
      .
    - >
      docker push 
      $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA 
      $CI_REGISTRY_IMAGE:latest
  only:
    - stag
  tags:
    - exabyte-docker
  variables:
    CI_REGISTRY_IMAGE: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME


prod-build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login ${CI_REGISTRY} -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD}
  script:
    - echo "$CI_COMMIT_SHORT_SHA" > version.txt
    - docker build -t ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:$CI_COMMIT_SHORT_SHA .
    - docker push ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:$CI_COMMIT_SHORT_SHA
    - docker tag ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:$CI_COMMIT_SHORT_SHA ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:stable
    - docker push ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:stable
  only:
    - /#prod/
  tags:
    - exabyte-docker

### TAGS ###
prod-tags:
  stage: tags
  image: docker
  services:
    - name: docker:dind
  environment: production
  before_script:
    - docker login ${CI_REGISTRY} -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD}
  script:
    - echo "$CI_COMMIT_TAG" > tag.txt
    - grep -E -o '[0-9]+(.[0-9]+)(.[0-9]+)-RELEASE' tag.txt > version.txt
    - docker pull ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:stable
    - docker tag ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:stable ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:$(cat version.txt)
    - docker push ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:$(cat version.txt)
  artifacts:
    paths:
    - version.txt
  only:
    - /#prod/
  tags:
    - exabyte-docker

### DEPLOY ###
stag-deploy:
  stage: deploy
  script:
    - docker-compose -p ${CI_PROJECT_NAME} down --rmi all
    - docker system prune -f
    - sed -i "s/apps_project-name/stag_hr_be_${CI_PROJECT_NAME}/g" "docker-compose.yml"
    - sed -i "s/cont_project-name/stag_hr_be_${CI_PROJECT_NAME}/g" "docker-compose.yml"
    - sed -i "s/project-name:latest/${CI_PROJECT_NAME}:latest/g" "docker-compose.yml"
    - sed -i "s/project-name.jar/${CI_PROJECT_NAME}.jar/g" "docker-compose.yml"
    - sed -i "s/8080:8080/${STAG_PORT_SERVICE}:8080/g" "docker-compose.yml"
    - sed -i "s/latest/${CI_COMMIT_SHORT_SHA}/g" "docker-compose.yml"
    - docker-compose up -d --no-build 
  only:
    - stag
  tags:
    - exabyte-shell

prod-deploy:
  stage: deploy
  script:
    - docker-compose -p ${CI_PROJECT_NAME} down --rmi all
    - docker system prune -f
    - sed -i "s/apps_project-name/prod_hr_be_${CI_PROJECT_NAME}/g" "docker-compose.yml"
    - sed -i "s/cont_project-name/prod_hr_be_${CI_PROJECT_NAME}/g" "docker-compose.yml"
    - sed -i "s/project-name:latest/${CI_PROJECT_NAME}:latest/g" "docker-compose.yml"
    - sed -i "s/project-name.jar/${CI_PROJECT_NAME}.jar/g" "docker-compose.yml"
    - sed -i "s/8080:8080/${PROD_PORT_SERVICE}:8080/g" "docker-compose.yml"
    - sed -i "s/latest/$(cat version.txt)/g" "docker-compose.yml"
    - docker-compose up -d --no-build 
  only:
    - /#prod/
  tags:
    - exabyte-shell
