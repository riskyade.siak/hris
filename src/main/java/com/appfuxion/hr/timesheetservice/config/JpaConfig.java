package com.appfuxion.hr.timesheetservice.config;

import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
public class JpaConfig {
}
