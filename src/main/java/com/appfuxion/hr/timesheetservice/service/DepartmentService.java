package com.appfuxion.hr.timesheetservice.service;

import com.appfuxion.hr.timesheetservice.exception.CommonException;
import com.appfuxion.hr.timesheetservice.model.Department;
import com.appfuxion.hr.timesheetservice.model.Employee;
import com.appfuxion.hr.timesheetservice.payload.CommonResponse;
import com.appfuxion.hr.timesheetservice.payload.request.AssignManager;
import com.appfuxion.hr.timesheetservice.payload.request.DepartmentRequest;
import com.appfuxion.hr.timesheetservice.payload.response.DepartmentResponse;
import com.appfuxion.hr.timesheetservice.repository.DepartmentRepository;
import com.appfuxion.hr.timesheetservice.repository.EmployeeRepository;
import com.appfuxion.hr.timesheetservice.util.CommonMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static com.appfuxion.hr.timesheetservice.service.EmployeeService.getEmployeeAsManager;

@Service
@Slf4j
public class DepartmentService {
    private final DepartmentRepository repository;
    private final EmployeeRepository employeeRepository;

    public DepartmentService(DepartmentRepository repository, EmployeeRepository employeeRepository) {
        this.repository = repository;
        this.employeeRepository = employeeRepository;
    }

    public CommonResponse addDataDepartment(DepartmentRequest request) {
        if (repository.existsByDepartmentName(request.getDepartmentName())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    CommonMessage.ALREADY_EXIST_MESSAGE,
                    String.format("Department %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getDepartmentName()),
                    HttpStatus.FORBIDDEN);
        }

        Department department = new Department();
        department.setDepartmentName(request.getDepartmentName());
        department.setManager(Objects.isNull(request.getManagerId()) ? null : this.getManagerById(request.getManagerId()));
        repository.save(department);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse listAllDepartments() {
        List<Department> departments = repository.findAll();
        List<DepartmentResponse> listDepartment = new ArrayList<>();
        departments.forEach(data -> listDepartment.add(new DepartmentResponse(
                data.getDepartmentId(), data.getDepartmentName(), !Objects.isNull(data.getManager()) ? data.getManager().getFullName() : null)
        ));

        if (listDepartment.size() > 1) {
            return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, listDepartment);
        } else {
            return new CommonResponse(CommonMessage.NO_DATA_CODE, CommonMessage.NO_DATA_MESSAGE, CommonMessage.NO_DATA_MESSAGE);
        }
    }

    public CommonResponse assignManager(AssignManager request) {
        Optional<Department> dataDepartment = repository.findById(request.getDepartmentId());
        AtomicReference<String> code = new AtomicReference<>();
        AtomicReference<String> message = new AtomicReference<>();
        AtomicReference<String> response = new AtomicReference<>();
        dataDepartment.ifPresentOrElse(data -> {
            Optional<Employee> dataEmployee = employeeRepository.findById(request.getManagerId());
            dataEmployee.ifPresentOrElse(manager -> {
                data.setManager(manager);
                repository.save(data);
                code.set(CommonMessage.SUCCESS_CODE);
                message.set(CommonMessage.SUCCESS_MESSAGE);
                response.set(CommonMessage.SUCCESS_MESSAGE);
            }, () -> {
                code.set(CommonMessage.NOT_FOUND_CODE);
                message.set(CommonMessage.NOT_FOUND_MESSAGE);
                response.set(String.format("Employee %s", CommonMessage.NOT_FOUND_MESSAGE));
            });
        }, () -> {
            code.set(CommonMessage.NOT_FOUND_CODE);
            message.set(CommonMessage.NOT_FOUND_MESSAGE);
            response.set(String.format("Department %s", CommonMessage.NOT_FOUND_MESSAGE));
        });
        return new CommonResponse(code.get(), message.get(), response.get());
    }

    public Department getDepartmentById(Long departmentId) {
        return repository.findById(departmentId).orElseThrow(() -> new CommonException(
                CommonMessage.NO_DATA_CODE,
                CommonMessage.NO_DATA_MESSAGE,
                String.format("%s for department your choose", CommonMessage.NO_DATA_MESSAGE),
                HttpStatus.NOT_ACCEPTABLE));
    }

    private Employee getManagerById(Long managerId) {
        return getEmployeeAsManager(managerId, employeeRepository);
    }
}
