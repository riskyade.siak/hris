package com.appfuxion.hr.timesheetservice.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record UserResponse(
        @JsonProperty(value = "user_id")
        Long userId,

        @JsonProperty(value = "full_name")
        String fullName,
        String email
) {
}
