package com.appfuxion.hr.timesheetservice.service;

import com.appfuxion.hr.timesheetservice.model.Attendance;
import com.appfuxion.hr.timesheetservice.payload.AttendanceLogInterface;
import com.appfuxion.hr.timesheetservice.payload.CommonResponse;
import com.appfuxion.hr.timesheetservice.payload.request.AttendanceLogForPeriodRequest;
import com.appfuxion.hr.timesheetservice.payload.request.AttendanceLogForPeriodWithStatusRequest;
import com.appfuxion.hr.timesheetservice.payload.request.AttendanceLogSummaryRequest;
import com.appfuxion.hr.timesheetservice.payload.response.AttendanceLogResponse;
import com.appfuxion.hr.timesheetservice.payload.response.AttendanceLogSummaryResponse;
import com.appfuxion.hr.timesheetservice.repository.AttendanceRepository;
import com.appfuxion.hr.timesheetservice.util.CommonUtil;
import com.appfuxion.hr.timesheetservice.util.EventType;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static java.time.DayOfWeek.*;

@Service
@Slf4j
public class AttendanceLogService {

    private final AttendanceRepository repository;
    private final EmployeeService employeeService;
    final Set<DayOfWeek> businessDays = Set.of(
            MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY
    );

    public AttendanceLogService(AttendanceRepository repository, EmployeeService employeeService) {
        this.repository = repository;
        this.employeeService = employeeService;
    }

    /*TODO refactor to accommodate different working hours*/
    public CommonResponse getAttendanceForPeriod(@Valid @NotNull AttendanceLogForPeriodRequest request) {
        if (request.getPaging().getPageNo() == 0 && request.getPaging().getPageSize() == 0)
            throw new IllegalArgumentException();
        Pageable pageable = PageRequest.of(request.getPaging().getPageNo(), request.getPaging().getPageSize(), request.getPaging().getDescending() ?
                Sort.by(request.getPaging().getOrderBy()).descending() : Sort.by(request.getPaging().getOrderBy()).ascending());
        List<String> statusList = CommonUtil.convertToListOfString(Arrays.asList(EventType.values()));
        Page<AttendanceLogInterface> attendanceForPeriod = repository.getAttendanceByDateRangeAndStatusWithPagination(statusList, LocalTime.of(8,0),
                LocalTime.of(16,0), getWorkingDays(request.getPeriod().getStart(),
                        request.getPeriod().getEnd()).toArray(new LocalDate[0]), pageable);
        AttendanceLogResponse response = AttendanceLogResponse.mapResponse(pageable, attendanceForPeriod);
        return new CommonResponse(null,null, response);
    }

    /*TODO refactor to accommodate different working hours*/
    public CommonResponse getAttendanceForPeriod(AttendanceLogForPeriodWithStatusRequest request) {
        Pageable pageable = PageRequest.of(request.getPaging().getPageNo(), request.getPaging().getPageSize(),
                request.getPaging().getDescending() ?
                    Sort.by(request.getPaging().getOrderBy()).descending() :
                        Sort.by(request.getPaging().getOrderBy()).ascending());
        Page<AttendanceLogInterface> attendanceForPeriod = repository.getAttendanceByDateRangeAndStatusWithPagination(
                CommonUtil.convertToListOfString(request.getStatusList()),
                LocalTime.of(8, 0), LocalTime.of(16, 0),
                getWorkingDays(request.getPeriod().getStart(),
                        request.getPeriod().getEnd()).toArray(new LocalDate[0]), pageable);
        AttendanceLogResponse response = AttendanceLogResponse.mapResponse(pageable, attendanceForPeriod);
        return new CommonResponse(null,null, response);
    }


    /*TODO refine to accommodate multi-country calendars etc.*/
    public List<LocalDate> getWorkingDays(LocalDate startDate, LocalDate endDate) {
        List<LocalDate> workingDays = new ArrayList<>();

        LocalDate currentDate = startDate;
        while (!currentDate.isAfter(endDate)) {
            DayOfWeek dayOfWeek = currentDate.getDayOfWeek();
            if (dayOfWeek != DayOfWeek.SATURDAY && dayOfWeek != DayOfWeek.SUNDAY) {
                workingDays.add(currentDate);
            }
            currentDate = currentDate.plusDays(1);
        }

        return workingDays;
    }

    public CommonResponse getAttendanceSummaryForPeriod(AttendanceLogSummaryRequest request) {
        List<Attendance> attendanceList= repository.getAttendanceByDateRange(
                request.getPeriod().getStart(),
                request.getPeriod().getEnd());

        long totalEmployees = employeeService.getNumberOfEmployees();
        long onTimeUsers = 0;
        long lateUsers = 0;
        long absentUsers = 0;

        List<LocalDate> workingDays = getWorkingDaysInMonthAscendingOrder(
                request.getPeriod().getStart(),
                request.getPeriod().getEnd());
        Map<LocalDate, List<Attendance>> attendanceOnParticularWorkingDayMap =
                attendanceOnParticularWorkingDay(attendanceList, workingDays);

        for (LocalDate workingDay: workingDays) {
            List<Attendance> attendanceOnParticularWorkingDay = attendanceOnParticularWorkingDayMap.get(workingDay);
            long absentUsersOnParticularWorkingDay = totalEmployees;
            for(Attendance attendance: attendanceOnParticularWorkingDay){
                LocalDateTime clockIn = attendance.getClockIn();
                LocalDateTime startOfWorkDay = LocalDateTime.of(
                        workingDay.getYear(), workingDay.getMonth(),
                        workingDay.getDayOfMonth(), 8, 0);

                if (clockIn!=null && clockIn.isBefore(startOfWorkDay)) {
                    onTimeUsers++;
                    absentUsersOnParticularWorkingDay--;
                } else if (clockIn!=null && clockIn.isAfter(startOfWorkDay)) {
                    lateUsers++;
                    absentUsersOnParticularWorkingDay--;
                }
            }
            absentUsers+=absentUsersOnParticularWorkingDay;
        }
        long noOfWorkingDays = workingDays.size();
        double divisor = totalEmployees * noOfWorkingDays * 100;
        divisor = divisor==0d? 1 : divisor;
        double onTimePercentage = onTimeUsers / divisor;
        double latePercentage = lateUsers / divisor;
        double absentPercentage = absentUsers / divisor;

        AttendanceLogSummaryResponse response = new AttendanceLogSummaryResponse(
                onTimeUsers, lateUsers, absentUsers,
                onTimePercentage, latePercentage, absentPercentage
        );

        return new CommonResponse(null,null, response);
    }

    private Map<LocalDate, List<Attendance>> attendanceOnParticularWorkingDay(
            List<Attendance> attendanceList, List<LocalDate> workingDays) {
        Map<LocalDate, List<Attendance>> attendanceMap = new HashMap<>();
        for (LocalDate workingDay : workingDays) {
            List<Attendance> attendanceOnParticularWorkingDay = new ArrayList<>();
            for (Attendance attendance : attendanceList) {
                if((!Objects.isNull(attendance.getClockIn()) &&
                        attendance.getClockIn().toLocalDate().isEqual(workingDay))
                        ||
                    (!Objects.isNull(attendance.getCreatedDate()) &&
                            attendance.getCreatedDate().toLocalDate().isEqual(workingDay))
                ) {
                    attendanceOnParticularWorkingDay.add(attendance);
                }
            }
            attendanceMap.put(workingDay, attendanceOnParticularWorkingDay);
        }
        return attendanceMap;
    }

    private List<LocalDate> getWorkingDaysInMonthAscendingOrder(LocalDate start, LocalDate end) {
        List<LocalDate> workingDays = new ArrayList<>();
        LocalDate current = start;
        while (current.isBefore(end)) {
            if (isWorkingDay(current)) {
                workingDays.add(current);
            }
            current = current.plusDays(1);
        }
        if (isWorkingDay(end)) {
            workingDays.add(end);
        }
        return workingDays;
    }

    private boolean isWorkingDay(LocalDate date) {
        return businessDays.contains(date.getDayOfWeek());
    }
}
