package com.appfuxion.hr.timesheetservice.payload;

import com.appfuxion.hr.timesheetservice.model.Attendance;
import com.appfuxion.hr.timesheetservice.model.Employee;
import com.appfuxion.hr.timesheetservice.model.Project;
import com.appfuxion.hr.timesheetservice.util.EventType;
import com.appfuxion.hr.timesheetservice.util.Workplace;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

public interface AttendanceLogInterface {
    Attendance getAttendance();
    UUID getAttendanceId();
    LocalDateTime getClockIn();
    LocalDateTime getClockOut();
    Workplace getWorkplace();
    EventType getEvent();
    Project getProject();
    String getDailyTask();
    Long getTotalWork();
    Employee getEmployee();
    LocalDate getAttendanceDate();

}
