package com.appfuxion.hr.timesheetservice.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class JobRequest implements Serializable {

    @NotBlank
    @JsonProperty(value = "job_title")
    private String jobTitle;

    @JsonProperty(value = "job_description")
    private String jobDescription;
}
