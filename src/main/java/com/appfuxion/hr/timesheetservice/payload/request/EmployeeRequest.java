package com.appfuxion.hr.timesheetservice.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class EmployeeRequest implements Serializable {

    @NotBlank
    @JsonProperty(value = "full_name")
    private String fullName;

    @NotBlank
    @Email
    @JsonProperty(value = "email")
    private String email;

    @JsonProperty(value = "phone_number")
    private String phoneNumber;

    @JsonProperty(value = "join_date")
    private Date joinDate;

    @JsonProperty(value = "job_id")
    private Long jobId;

    @JsonProperty(value = "department_id")
    private Long departmentId;

    @JsonProperty(value = "project_id_list")
    private Set<Long> projectIdList;

    @JsonProperty(value = "client_id")
    private Long clientId;
}
