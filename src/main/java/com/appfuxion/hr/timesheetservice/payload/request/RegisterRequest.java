package com.appfuxion.hr.timesheetservice.payload.request;

import com.appfuxion.hr.timesheetservice.util.Password;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class RegisterRequest implements Serializable {

    @NotNull
    @NotEmpty
    @NotBlank
    @JsonProperty(value = "full_name")
    private String fullName;

    @NotNull
    @NotEmpty
    @NotBlank
    @Email
    private String email;

    @NotNull
    @NotEmpty
    @NotBlank
    @Password
    private String password;
}
