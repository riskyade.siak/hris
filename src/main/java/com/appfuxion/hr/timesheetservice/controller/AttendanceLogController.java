package com.appfuxion.hr.timesheetservice.controller;

import com.appfuxion.hr.timesheetservice.payload.CommonResponse;
import com.appfuxion.hr.timesheetservice.payload.request.AttendanceLogForPeriodRequest;
import com.appfuxion.hr.timesheetservice.payload.request.AttendanceLogForPeriodWithStatusRequest;
import com.appfuxion.hr.timesheetservice.payload.request.AttendanceLogSummaryRequest;
import com.appfuxion.hr.timesheetservice.service.AttendanceLogService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("attendance-log")
@Slf4j
public class AttendanceLogController {

    private final AttendanceLogService service;

    public AttendanceLogController(AttendanceLogService service) {
        this.service = service;
    }

    @PostMapping("/for-period")
    public ResponseEntity<CommonResponse> getAttendanceForPeriod(
            @RequestBody AttendanceLogForPeriodRequest request
            ) {
        log.info(String.format("Entering method getAttendanceForPeriod on class %s", AttendanceLogController.class.getName()));
        CommonResponse response = service.getAttendanceForPeriod(request);
        log.info(String.format("Exiting method getAttendanceForPeriod on class %s", AttendanceLogController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/for-period-with-status")
    public ResponseEntity<CommonResponse> getAttendanceForPeriodAndStatus(
            @Valid @RequestBody AttendanceLogForPeriodWithStatusRequest request) {
        log.info(String.format("Entering method getAttendanceForPeriodAndStatus on class %s", AttendanceLogController.class.getName()));
        CommonResponse response = service.getAttendanceForPeriod(request);
        log.info(String.format("Exiting method getAttendanceForPeriodAndStatus on class %s", AttendanceLogController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/summary-for-period")
    public ResponseEntity<CommonResponse> getAttendanceSummaryForPeriod(
            @Valid @RequestBody AttendanceLogSummaryRequest request) {
        log.info(String.format("Entering method getAttendanceSummaryForPeriod on class %s", AttendanceLogController.class.getName()));
        CommonResponse response = service.getAttendanceSummaryForPeriod(request);
        log.info(String.format("Exiting method getAttendanceSummaryForPeriod on class %s", AttendanceLogController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
