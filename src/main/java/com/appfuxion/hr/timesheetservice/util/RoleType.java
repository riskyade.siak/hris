package com.appfuxion.hr.timesheetservice.util;

public enum RoleType {
    ROLE_USER, ROLE_ADMIN, ROLE_HR
}
