package com.appfuxion.hr.timesheetservice.service;


import com.appfuxion.hr.timesheetservice.dto.TimesheetDTO;
import com.appfuxion.hr.timesheetservice.util.TimesheetClientCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@Slf4j
public class TimesheetExportService {

    private final AttendanceService AttendanceService;
    private final EmployeeService employeeService;
    private final ProjectService projectService;

    public TimesheetExportService(AttendanceService AttendanceService, EmployeeService employeeService, ProjectService projectService) {
        this.AttendanceService = AttendanceService;
        this.employeeService = employeeService;
        this.projectService = projectService;
    }


    public ByteArrayInputStream exportTimesheet(List<TimesheetDTO> timesheetEntries) {
        try (

            InputStream templateInputStream = getTemplateInputStream(timesheetEntries.get(0).getClientCode());
            XSSFWorkbook workbook = new XSSFWorkbook(templateInputStream)) {

            Sheet sheet = workbook.getSheet("Timesheet");
            fillData(sheet, timesheetEntries);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return new ByteArrayInputStream(outputStream.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("Failed to export timesheet data to Excel", e);
        }
    }

    private void fillData(Sheet sheet, List<TimesheetDTO> timesheetEntries) {

        if(timesheetEntries.get(0).getClientCode() == null || timesheetEntries.get(0).getClientCode() == TimesheetClientCode.OTHER){
            //Header
            sheet.getRow(5).getCell(3).setCellValue(": "+timesheetEntries.get(0).getEmployeeName());
            sheet.getRow(6).getCell(3).setCellValue(": "+timesheetEntries.get(0).getDate());
            sheet.getRow(7).getCell(3).setCellValue(": "+timesheetEntries.get(timesheetEntries.size()-1).getDate());

            int rowNum = 11;
            for (TimesheetDTO entry : timesheetEntries) {
                Row row = sheet.getRow(rowNum++);
                row.getCell(0).setCellValue(rowNum);
                row.getCell(2).setCellValue(entry.getDate().format(DateTimeFormatter.ofPattern("E")));
                row.getCell(3).setCellValue(entry.getDate());
                row.getCell(4).setCellValue(entry.getTask());
                if(!entry.getProjectName().isEmpty()){
                    row.getCell(4).setCellValue("In Project");
                    row.getCell(5).setCellValue(entry.getProjectName());
                }
                row.getCell(6).setCellValue(entry.getTask());
                row.getCell(7).setCellValue(entry.getDetail());

                row.getCell(9).setCellValue(entry.getClockIn().format(DateTimeFormatter.ofPattern("HH:mm")));
                row.getCell(10).setCellValue(entry.getClockOut().format(DateTimeFormatter.ofPattern("HH:mm")));
                row.getCell(11).setCellValue(entry.getHoursWorked());
            }
        }
        
    }

    private InputStream getTemplateInputStream(TimesheetClientCode clientCode) throws IOException {
        String templatePath = "templates\\timesheet\\" + clientCode.toString() + ".xlsx";
        return new ClassPathResource(templatePath).getInputStream();
    }

}