package com.appfuxion.hr.timesheetservice.payload.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttendanceLogForPeriodRequest implements Serializable {
    PeriodRequest period = new PeriodRequest();
    PagingRequest paging = new PagingRequest();
}
