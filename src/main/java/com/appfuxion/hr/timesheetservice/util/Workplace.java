package com.appfuxion.hr.timesheetservice.util;

public enum Workplace {
    WFO, WFH
}
