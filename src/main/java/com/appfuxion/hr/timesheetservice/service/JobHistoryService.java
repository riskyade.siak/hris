package com.appfuxion.hr.timesheetservice.service;

import com.appfuxion.hr.timesheetservice.repository.JobHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class JobHistoryService {

    private final JobHistoryRepository repository;

    public JobHistoryService(JobHistoryRepository repository) {
        this.repository = repository;
    }
}
