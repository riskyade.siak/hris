package com.appfuxion.hr.timesheetservice.payload.response;

import com.appfuxion.hr.timesheetservice.payload.AttendanceLog;
import com.appfuxion.hr.timesheetservice.payload.AttendanceLogInterface;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttendanceLogResponse implements Serializable {

    Page<AttendanceResponse> attendances = null;

    public static AttendanceLogResponse mapResponse(Pageable pageable,
                                                    AttendanceLog attendanceLog) {
        AttendanceLogResponse attendanceLogResponse = new AttendanceLogResponse();
        attendanceLogResponse.setAttendances(
                Objects.isNull(attendanceLog.getAttendances())?
                        new PageImpl<>(new ArrayList<>(), pageable, 0):
                        attendanceLog.getAttendances()
                                .map(AttendanceResponse::mapResponse));
        return attendanceLogResponse;
    }
    public static AttendanceLogResponse mapResponse(Pageable pageable,
                                                    Page<AttendanceLogInterface> attendanceLogInterface) {
        AttendanceLogResponse attendanceLogResponse = new AttendanceLogResponse();
        attendanceLogResponse.setAttendances(Objects.isNull(attendanceLogInterface)?
                new PageImpl<>(new ArrayList<>(),pageable, 0):
                    attendanceLogInterface
                            .map(AttendanceLogResponse::mapResponse));
        return attendanceLogResponse;
    }
    public static AttendanceResponse mapResponse(AttendanceLogInterface attendanceLog) {
        AttendanceResponse attendanceLogResponse = new AttendanceResponse();
        attendanceLogResponse.setAttendanceId(attendanceLog.getAttendanceId());
        attendanceLogResponse.setClockIn(attendanceLog.getClockIn());
        attendanceLogResponse.setClockOut(attendanceLog.getClockOut());
        attendanceLogResponse.setWorkplace(attendanceLog.getWorkplace());
        attendanceLogResponse.setEvent(attendanceLog.getEvent());
        attendanceLogResponse.setProject(
                Objects.isNull(attendanceLog.getProject())?null: new ProjectResponse(
                        attendanceLog.getProject().getProjectId(), attendanceLog.getProject().getProjectName(),
                        Objects.isNull(
                                attendanceLog.getProject().getManager()) ? null :
                                attendanceLog.getProject().getManager().getFullName()));
        attendanceLogResponse.setDailyTask(attendanceLog.getDailyTask());
        attendanceLogResponse.setTotalWork(attendanceLog.getTotalWork());
        attendanceLogResponse.setEmployee(
                Objects.isNull(
                        attendanceLog.getEmployee())? null :
                        EmployeeResponse.mapResponse(attendanceLog.getEmployee()));
        attendanceLogResponse.setAttendanceDate(attendanceLog.getAttendanceDate());
        return attendanceLogResponse;
    }
}
