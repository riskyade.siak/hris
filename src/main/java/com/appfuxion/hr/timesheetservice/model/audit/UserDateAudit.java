package com.appfuxion.hr.timesheetservice.model.audit;

import com.appfuxion.hr.timesheetservice.util.CommonUtil;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.auditing.config.AuditingConfiguration;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@EntityListeners(AuditingConfiguration.class)
@MappedSuperclass
public class UserDateAudit implements Serializable {
    @CreatedDate
    @Column(name = "created_date", nullable = false, updatable = false)
    private LocalDateTime createdDate;

    private LocalDateTime updatedDate;

    @CreatedBy
    @Column(name = "created_by", nullable = false, updatable = false)
    private String createdBy;

    private String updatedBy;

    @PrePersist
    public void prePersist() {
        this.setCreatedDate(LocalDateTime.now());
        this.setCreatedBy(CommonUtil.currentUser());
    }

    @PreUpdate
    public void preUpdate() {
        this.setUpdatedDate(LocalDateTime.now());
        this.setUpdatedBy(CommonUtil.currentUser());
    }
}
