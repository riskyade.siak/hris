package com.appfuxion.hr.timesheetservice.config;

import com.appfuxion.hr.timesheetservice.util.CommonConstant;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessagingConfig {

    @Bean
    public Queue registerQueue() {
        return new Queue(CommonConstant.REGISTER_QUEUE);
    }

    @Bean
    public TopicExchange registerExchange() {
        return new TopicExchange(CommonConstant.REGISTER_EXCHANGE);
    }

    @Bean
    public Binding registerBinding() {
        return BindingBuilder.bind(registerQueue()).to(registerExchange()).with(CommonConstant.REGISTER_ROUTING_KEY);
    }

    @Bean
    public Queue clockInQueue() {
        return new Queue(CommonConstant.CLOCK_IN_QUEUE);
    }

    @Bean
    public TopicExchange clockInExchange() {
        return new TopicExchange(CommonConstant.CLOCK_IN_EXCHANGE);
    }

    @Bean
    public Binding clockInBinding() {
        return BindingBuilder.bind(clockInQueue()).to(clockInExchange()).with(CommonConstant.CLOCK_IN_ROUTING_KEY);
    }

    @Bean
    public Queue clockOutQueue() {
        return new Queue(CommonConstant.CLOCK_OUT_QUEUE);
    }

    @Bean
    public TopicExchange clockOutExchange() {
        return new TopicExchange(CommonConstant.CLOCK_OUT_EXCHANGE);
    }

    @Bean
    public Binding clockOutBinding() {
        return BindingBuilder.bind(clockOutQueue()).to(clockOutExchange()).with(CommonConstant.CLOCK_OUT_ROUTING_KEY);
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost");
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        return connectionFactory;
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}