package com.appfuxion.hr.timesheetservice.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record ProjectResponse(
        @JsonProperty(value = "project_id")
        Long projectId,

        @JsonProperty(value = "project_name")
        String projectName,

        @JsonProperty(value = "manager")
        String manager) implements Serializable {
}
