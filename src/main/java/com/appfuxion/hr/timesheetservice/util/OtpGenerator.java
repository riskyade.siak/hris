package com.appfuxion.hr.timesheetservice.util;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Component;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Component
public class OtpGenerator {
    private static final Integer EXPIRE_MIN = 5;
    private final LoadingCache<String, Integer> otpCache;
    private final Random random = SecureRandom.getInstanceStrong();

    /**
     * Constructor configuration.
     */
    public OtpGenerator() throws NoSuchAlgorithmException {
        super();
        otpCache = CacheBuilder.newBuilder()
                .expireAfterWrite(EXPIRE_MIN, TimeUnit.MINUTES)
                .build(new CacheLoader<>() {
                    @Override
                    public Integer load(String s) {
                        return 0;
                    }
                });
    }

    /**
     * Method for generating OTP and put it in cache.
     *
     * @param key - cache key
     * @return cache value (generated OTP number)
     */
    public String generateOTP(String key) {
        int otp = 1000 + random.nextInt(9000);
        otpCache.put(key, otp);
        return String.valueOf(otp);
    }

    /**
     * Method for getting OTP value by key.
     *
     * @param key - target key
     * @return OTP value
     */
    public String getOPTByKey(String key) {
        return String.valueOf(otpCache.getIfPresent(key));
    }

    /**
     * Method for removing key from cache.
     *
     * @param key - target key
     */
    public void clearOTPFromCache(String key) {
        otpCache.invalidate(key);
    }
}
