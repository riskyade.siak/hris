package com.appfuxion.hr.timesheetservice.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ForgotPasswordRequest implements Serializable {
    private String email;
}
