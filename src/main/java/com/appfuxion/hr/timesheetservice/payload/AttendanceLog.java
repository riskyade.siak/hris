package com.appfuxion.hr.timesheetservice.payload;

import com.appfuxion.hr.timesheetservice.model.Attendance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AttendanceLog {
    Page<Attendance> attendances;
}
