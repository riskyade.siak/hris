package com.appfuxion.hr.timesheetservice.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record DepartmentResponse(
        @JsonProperty(value = "department_id")
        Long departmentId,

        @JsonProperty(value = "department_name")
        String departmentName,

        @JsonProperty(value = "manager")
        String manager) implements Serializable {
}
