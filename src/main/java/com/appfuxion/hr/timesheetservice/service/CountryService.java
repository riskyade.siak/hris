package com.appfuxion.hr.timesheetservice.service;

import com.appfuxion.hr.timesheetservice.exception.CommonException;
import com.appfuxion.hr.timesheetservice.model.Country;
import com.appfuxion.hr.timesheetservice.payload.CommonResponse;
import com.appfuxion.hr.timesheetservice.payload.response.CountryResponse;
import com.appfuxion.hr.timesheetservice.repository.CountryRepository;
import com.appfuxion.hr.timesheetservice.util.CommonMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class CountryService {

    private final CountryRepository repository;

    public CountryService(CountryRepository repository) {
        this.repository = repository;
    }

    public Country getCountryById(Long countryId) {
        return repository.findById(countryId).orElseThrow(() -> new CommonException(
                CommonMessage.NO_DATA_CODE,
                CommonMessage.NO_DATA_MESSAGE,
                String.format("%s for country your choose", CommonMessage.NO_DATA_MESSAGE),
                HttpStatus.NOT_ACCEPTABLE));
    }

    public CommonResponse listAllCountries() {
        List<CountryResponse> responses = new ArrayList<>();
        repository.findAll().forEach(country -> responses.add(new CountryResponse(
                country.getCountryId(),
                country.getCountryName(),
                country.getAlpha2Code(),
                country.getAlpha3Code(),
                country.getNumericCode())));

        if (responses.isEmpty()) {
            return new CommonResponse(
                    CommonMessage.NO_DATA_CODE,
                    CommonMessage.NO_DATA_MESSAGE,
                    CommonMessage.NO_DATA_MESSAGE
            );
        } else {
            return new CommonResponse(
                    CommonMessage.SUCCESS_CODE,
                    CommonMessage.SUCCESS_MESSAGE,
                    responses
            );
        }
    }
}
