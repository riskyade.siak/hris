package com.appfuxion.hr.timesheetservice.util;

public enum TimesheetClientCode {
    ICM, ITG, MBB, OTHER
}
