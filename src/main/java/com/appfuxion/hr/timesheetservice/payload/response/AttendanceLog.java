package com.appfuxion.hr.timesheetservice.payload.response;

import com.appfuxion.hr.timesheetservice.model.Employee;
import com.appfuxion.hr.timesheetservice.model.Project;
import com.appfuxion.hr.timesheetservice.util.EventType;
import com.appfuxion.hr.timesheetservice.util.Workplace;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttendanceLog {
    @JsonProperty(value = "attendance_id")
    private UUID attendanceId;

    @JsonProperty(value = "clockIn")
    private LocalDateTime clockIn;

    @JsonProperty(value = "clockOut")
    private LocalDateTime clockOut;

    @JsonProperty(value = "workplace")
    private Workplace workplace;

    @JsonProperty(value = "event")
    private EventType event;

    private Project project;

    private String dailyTask;

    private Long totalWork;

    private Employee employee;

    @JsonProperty(value = "attendanceDate")
    private LocalDate attendanceDate;

    private LocalDateTime createdDate;

}
