package com.appfuxion.hr.timesheetservice.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class AttendanceRequest implements Serializable {

    @JsonProperty(value = "attendance_id")
    private String attendanceId;

    @JsonProperty(value = "clock_in")
    private LocalDateTime clockIn;

    @JsonProperty(value = "clock_out")
    private LocalDateTime clockOut;

    @NotNull
    @JsonProperty(value = "workplace")
    private String workplace;

    @NotNull
    @JsonProperty(value = "daily_task")
    private String dailyTask;

    @JsonProperty(value = "project_id")
    @NotNull
    private Long projectId;
    @JsonProperty(value = "attendance_date")
    private LocalDate attendanceDate;
}
