FROM eclipse-temurin:17-jdk-alpine

WORKDIR /app
ARG JAR_FILE
ADD target/timesheet-service-0.0.1.jar /app/timesheet-service.jar
EXPOSE 8080

ENTRYPOINT ["java","-jar","/app/timesheet-service.jar"]