package com.appfuxion.hr.timesheetservice.service;

import com.appfuxion.hr.timesheetservice.exception.CommonException;
import com.appfuxion.hr.timesheetservice.model.Employee;
import com.appfuxion.hr.timesheetservice.model.auth.Token;
import com.appfuxion.hr.timesheetservice.model.auth.User;
import com.appfuxion.hr.timesheetservice.payload.CommonResponse;
import com.appfuxion.hr.timesheetservice.payload.request.*;
import com.appfuxion.hr.timesheetservice.payload.response.*;
import com.appfuxion.hr.timesheetservice.repository.EmployeeProjectRepository;
import com.appfuxion.hr.timesheetservice.repository.TokenRepository;
import com.appfuxion.hr.timesheetservice.repository.UserRepository;
import com.appfuxion.hr.timesheetservice.security.JwtService;
import com.appfuxion.hr.timesheetservice.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository repository;
    private final TokenRepository tokenRepository;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final EmployeeService employeeService;
    private final EmployeeProjectRepository employeeProjectRepository;
    private final OtpGenerator otpGenerator;
    private final RestTemplate restTemplate;
    private final PasswordEncoder passwordEncoder;

    public CommonResponse authenticate(AuthenticationRequest request) {
        User user = emailValidator(request.getEmail());

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );

        if (!authentication.isAuthenticated()) {
            throw new CommonException(
                    CommonMessage.INVALID_AUTH_CODE,
                    CommonMessage.INVALID_AUTH_MESSAGE,
                    CommonMessage.INVALID_AUTH_MESSAGE,
                    HttpStatus.UNAUTHORIZED
            );
        }

        String jwtToken = jwtService.generateToken(user);
        String refreshToken = jwtService.generateRefreshToken(user);
        revokeAllUserTokens(user);
        saveUserToken(user, jwtToken);

        AuthenticationResponse response;

        if (!user.getRole().equals(RoleType.ROLE_ADMIN)) {
            Employee dataEmployee = employeeService.getEmployeeByEmail(request.getEmail());
            EmployeeResponse employee = new EmployeeResponse();
            employee.setEmployeeId(dataEmployee.getEmployeeId());
            employee.setFullName(dataEmployee.getFullName());
            employee.setEmail(dataEmployee.getEmail());
            employee.setPhoneNumber(dataEmployee.getPhoneNumber());
            employee.setJoinDate(dataEmployee.getJoinDate());
            employee.setJobTitle(dataEmployee.getJob().getJobTitle());

            ClientResponse client = new ClientResponse(
                    dataEmployee.getClient().getClientId(),
                    dataEmployee.getClient().getClientName(),
                    employee.getEmail(),
                    dataEmployee.getClient().getNotes());

            Set<ProjectResponse> projectList = new HashSet<>();
            employeeProjectRepository.findByEmployeeAndIsActive(dataEmployee, true).forEach(employeeProject -> {
                ProjectResponse project = new ProjectResponse(
                        employeeProject.getProject().getProjectId(),
                        employeeProject.getProject().getProjectName(),
                        Objects.isNull(employeeProject.getProject().getManager()) ? null: employeeProject.getProject().getManager().getFullName()
                );
                projectList.add(project);
            });

            response = new AuthenticationResponse(jwtToken, refreshToken, client, employee, projectList, user.getRole().toString().toLowerCase(), null);
        } else {
            response = new AuthenticationResponse(
                    jwtToken,
                    refreshToken,
                    null,
                    null,
                    null,
                    user.getRole().toString().toLowerCase(),
                    new UserResponse(user.getUserId(), user.getFullName(), user.getEmail()));
        }

        return new CommonResponse(
                CommonMessage.SUCCESS_CODE,
                CommonMessage.SUCCESS_MESSAGE,
                response);
    }

    public CommonResponse requestForgotPassword(ForgotPasswordRequest request) throws URISyntaxException {
        User user = emailValidator(request.getEmail());
        String otp = otpGenerator.generateOTP(user.getEmail());

        Employee employee = employeeService.getEmployeeByEmail(request.getEmail());

        // TODO Change to message broker
        URI uri = new URI("http://stag_hr_be_push-notification-service:8080/v1/push-notification/forgot-password");


        ChangePasswordRequest changePassword = new ChangePasswordRequest();
        changePassword.setEmail(employee.getEmail());
        changePassword.setOtpCode(otp);
        changePassword.setName(employee.getFullName());

        restTemplate.postForEntity(uri, changePassword, String.class);

        return new CommonResponse(
                CommonMessage.SUCCESS_CODE,
                CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse validateOtp(ValidateOtpRequest request) {
        User user = emailValidator(request.getEmail());

        String otpCode = otpGenerator.getOPTByKey(user.getEmail());

        if (!request.getOtpCode().equalsIgnoreCase(otpCode)) {
            throw new CommonException(
                    CommonMessage.INVALID_CODE,
                    CommonMessage.INVALID_MESSAGE,
                    String.format("OTP Code is %s ".concat(CommonMessage.INVALID_MESSAGE), request.getOtpCode()),
                    HttpStatus.FORBIDDEN
            );
        }

        return new CommonResponse(
                CommonMessage.SUCCESS_CODE,
                CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse changePassword(NewPasswordRequest request) {
        User user = emailValidator(request.getEmail());

        if (CommonValidator.passwordIsValid(request.getPassword())) {
            user.setPassword(passwordEncoder.encode(request.getPassword()));
            repository.save(user);
        } else {
            throw new CommonException(
                    CommonMessage.INVALID_CODE,
                    CommonMessage.INVALID_MESSAGE,
                    String.format("The password your enter is %s", CommonMessage.INVALID_MESSAGE),
                    HttpStatus.FORBIDDEN);
        }

        return new CommonResponse(
                CommonMessage.SUCCESS_CODE,
                CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    private User emailValidator(String email) {
        return repository.findByEmail(email)
                .orElseThrow(() -> new CommonException(
                        CommonMessage.NOT_FOUND_CODE,
                        CommonMessage.NOT_FOUND_MESSAGE,
                        String.format("Email %s ".concat(CommonMessage.NOT_FOUND_MESSAGE), email),
                        HttpStatus.FORBIDDEN
                ));
    }

    private void saveUserToken(User user, String jwtToken) {
        Token token = Token.builder()
                .user(user)
                .token(jwtToken)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false)
                .build();
        tokenRepository.save(token);
    }

    private void revokeAllUserTokens(User user) {
        var validUserTokens = tokenRepository.findAllValidTokenByUser(user.getUserId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }

    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken;
        final String userEmail;
        if (authHeader == null ||!authHeader.startsWith("Bearer ")) {
            return;
        }
        refreshToken = authHeader.substring(7);
        userEmail = jwtService.extractUsername(refreshToken);
        if (userEmail != null) {
            User user = this.repository.findByEmail(userEmail)
                    .orElseThrow();
            if (jwtService.isTokenValid(refreshToken, user)) {
                String accessToken = jwtService.generateToken(user);
                revokeAllUserTokens(user);
                saveUserToken(user, accessToken);

                Employee dataEmployee = employeeService.getEmployeeByEmail(userEmail);
                EmployeeResponse employee = new EmployeeResponse();
                employee.setFullName(dataEmployee.getFullName());
                employee.setEmail(dataEmployee.getFullName());
                employee.setPhoneNumber(dataEmployee.getPhoneNumber());
                employee.setJoinDate(dataEmployee.getJoinDate());
                employee.setJobTitle(dataEmployee.getJob().getJobTitle());

                ClientResponse client = new ClientResponse(
                        dataEmployee.getClient().getClientId(),
                        dataEmployee.getClient().getClientName(),
                        dataEmployee.getClient().getEmail(),
                        dataEmployee.getClient().getNotes());

                Set<ProjectResponse> projectList = new HashSet<>();
                employeeProjectRepository.findByEmployeeAndIsActive(dataEmployee, true).forEach(employeeProject -> {
                    ProjectResponse responses = new ProjectResponse(
                            employeeProject.getProject().getProjectId(),
                            employeeProject.getProject().getProjectName(),
                            Objects.isNull(employeeProject.getProject().getManager()) ? null: employeeProject.getProject().getManager().getFullName()
                    );
                    projectList.add(responses);
                });

                new ObjectMapper().writeValue(response.getOutputStream(),
                        new AuthenticationResponse(accessToken, refreshToken, client, employee, projectList, user.getRole().toString().toLowerCase(), null));
            }
        }
    }

    public CommonResponse register(RegisterRequest request) {
        if (repository.existsByEmail(request.getEmail())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    CommonMessage.ALREADY_EXIST_MESSAGE,
                    String.format("Email %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getEmail()),
                    HttpStatus.FORBIDDEN);
        }

        User user = User.builder()
                .fullName(request.getFullName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(RoleType.ROLE_ADMIN)
                .build();
        repository.save(user);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }
}
