package com.appfuxion.hr.timesheetservice.service;

import com.appfuxion.hr.timesheetservice.exception.CommonException;
import com.appfuxion.hr.timesheetservice.model.Client;
import com.appfuxion.hr.timesheetservice.model.Location;
import com.appfuxion.hr.timesheetservice.payload.CommonResponse;
import com.appfuxion.hr.timesheetservice.payload.request.ClientRequest;
import com.appfuxion.hr.timesheetservice.payload.response.ClientResponse;
import com.appfuxion.hr.timesheetservice.repository.ClientRepository;
import com.appfuxion.hr.timesheetservice.repository.LocationRepository;
import com.appfuxion.hr.timesheetservice.util.CommonMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class ClientService {

    private final ClientRepository repository;
    private final CountryService countryService;
    private final LocationRepository locationRepository;

    public ClientService(ClientRepository repository, CountryService countryService, LocationRepository locationRepository) {
        this.repository = repository;
        this.countryService = countryService;
        this.locationRepository = locationRepository;
    }

    public CommonResponse addDataClient(ClientRequest request) {
        if (repository.existsByClientName(request.getClientName())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    CommonMessage.ALREADY_EXIST_MESSAGE,
                    String.format("Client Name %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getClientName()),
                    HttpStatus.FORBIDDEN);
        }

        Client client = new Client();
        client.setClientName(request.getClientName());
        client.setEmail(request.getEmail());
        client.setNotes(request.getNotes());

        if (!Objects.isNull(request.getLocation())) {
            Location location = new Location();
            location.setStreetAddress(request.getLocation().getStreetAddress());
            location.setPostalCode(request.getLocation().getPostalCode());
            location.setCity(request.getLocation().getCity());
            location.setProvince(request.getLocation().getProvince());
            location.setCountry(countryService.getCountryById(request.getLocation().getCountryId()));
            locationRepository.saveAndFlush(location);

            Location newLocation = locationRepository.findByStreetAddress(location.getStreetAddress())
                            .orElseThrow();

            client.setLocation(newLocation);
        } else {
            client.setLocation(null);
        }
        repository.save(client);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse listAllClients() {
        List<Client> clientList = repository.findAll();
        List<ClientResponse> clients = new ArrayList<>();
        clientList.forEach(data -> clients.add(new ClientResponse(
                data.getClientId(), data.getClientName(), data.getEmail(), data.getNotes()
        )));

        if (clients.size() > 1) {
            return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, clients);
        } else {
            return new CommonResponse(CommonMessage.NO_DATA_CODE, CommonMessage.NO_DATA_MESSAGE, CommonMessage.NO_DATA_MESSAGE);
        }
    }

    public Client getClientById(Long clientId) {
        return repository.findById(clientId).orElseThrow(() -> new CommonException(
                CommonMessage.NO_DATA_CODE,
                CommonMessage.NO_DATA_MESSAGE,
                String.format("%s for client your choose", CommonMessage.NO_DATA_MESSAGE),
                HttpStatus.NOT_ACCEPTABLE));
    }
}
